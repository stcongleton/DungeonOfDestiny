﻿using System;

namespace Engine
{
    public class Map
    {
        public Room[,] map { get; set; }
        Random rand = new Random(Guid.NewGuid().GetHashCode());
        public int currentX { get; set; } = 0;
        public int currentY { get; set; } = 0;
        int X, Y;
        int mX, mY;

        public Map(int maxX, int maxY, int maxMonster, int maxItem)
        {
            X = maxX - 1;
            Y = maxY - 1;
            mX = maxX;
            mY = maxY;

            map = new Room[maxY, maxX];
            for (int i = 0; i <= Y; i++)
            {
                for (int j = 0; j <= X; j++)
                {
                    map[i, j] = new Room(i, j, maxMonster, maxItem);
                    checkWalls(j, i);
                }
            }
            //Open both available exits from the origin room.
            map[0, 0].South = true;
            map[0, 0].East = true;

            for (int a = 0; a <= 3; a++)
            {
                for (int i = 0; i <= Y; i++)
                {
                    for (int j = 0; j <= X; j++)
                    {
                        checkSurrounding(i, j);
                    }
                }
            }
            for (int a = 0; a <= 4; a++)
            {
                for (int i = 0; i <= Y; i++)
                {
                    for (int j = 0; j <= X; j++)
                    {
                        findPathToStart(i, j);
                    }
                }
            }

            findBoss();

        }

        void findBoss()
        {

            int count = 0;
            bool bossSet = false;

            while (!bossSet)
            {
                int x = rand.Next(0, X);
                int y = rand.Next(0, Y);

                if (!map[y, x].pathToStart)
                {
                    map[y, x].boss = false;
                    count += 1;
                }
                else if (x == 0 && y == 0)
                {
                    map[y, x].boss = false;
                    count += 1;
                }
                else
                {
                    map[y, x].boss = true;
                    map[y, x].makeBoss();
                    map[y, x].monster = false;
                    map[y, x].bossReference.monsterName = "BOSS " + map[y, x].bossReference.monsterName;
                    bossSet = true;
                }
            }


        }

        void checkNorth(int y, int x)
        {
            if (y > 0)
            {
                if (map[y - 1, x].South == true)
                {
                    map[y, x].North = true;
                }
                else
                {
                    map[y, x].North = false;
                }
            }
        }

        void checkEast(int y, int x)
        {
            if (x > 0)
            {
                if (map[y, x - 1].East)
                {
                    map[y, x].West = true;
                }
                else
                {
                    map[y, x].West = false;
                }
            }
        }

        public void checkSurrounding(int y, int x)
        {
            checkNorth(y, x);
            checkEast(y, x);
        }

        void findPathToStart(int y, int x)
        {
            //The start room has a path to itself for sure.
            map[0, 0].pathToStart = true;
            if (map[y, x].North == true)
            {
                if (map[y - 1, x].pathToStart)
                {
                    map[y, x].pathToStart = true;
                }
            }

            if (map[y, x].South == true)
            {
                if (map[y + 1, x].pathToStart)
                {
                    map[y, x].pathToStart = true;
                }
            }
            if (map[y, x].East == true)
            {
                if (map[y, x + 1].pathToStart)
                {
                    map[y, x].pathToStart = true;
                }
            }
            if (map[y, x].West)
            {
                if (map[y, x - 1].pathToStart)
                {
                    map[y, x].pathToStart = true;
                }
            }

        }

        void checkWalls(int x, int y)
        {
            //Make sure there are no openings anywhere there shouldn't be.
            //(I.E. Dont fly off the edge of the world.)
            if (y == 0)
            {
                map[y, x].North = false;
            }

            if (y == Y)
            {
                map[y, x].South = false;
            }
            if (x == X)
            {
                map[y, x].East = false;
            }
            if (x == 0)
            {
                map[y, x].West = false;
            }
        }

        public void printMap()
        {
            //Make the map print on the screen.
            //This method was a bitch to write and is hacky as hell.
            char[,][,] roomMap = new char[mX, mY][,];

            //Generate the room's array.
            for (int a = 0; a <= X; a++)
            {
                for (int b = 0; b <= Y; b++)
                {
                    roomMap[a, b] = map[b , a].generateRoom();
                }
            }

            for (int a = 0; a <= Y; a++)
            {
                //First Row of the map.
                for (int b = 0; b <= X; b++)
                {
                    for (int c = 0; c <= 4; c++)
                    {

                        Console.Write(checkCharacter(roomMap[b, a][0, c]));
                        Console.Write(' ');
                        Console.ResetColor();

                    }
                }
                Console.WriteLine();

                //Second Row of the map.
                for (int b = 0; b <= X; b++)
                {
                    for (int c = 0; c <= 4; c++)
                    {
                        Console.Write(checkCharacter(roomMap[b, a][1, c]));
                        Console.Write(' ');
                        Console.ResetColor();
                    }
                }

                //Third Row of the map.
                Console.WriteLine();
                for (int b = 0; b <= X; b++)
                {
                    for (int c = 0; c <= 4; c++)
                    {

                        Console.Write(checkCharacter(roomMap[b, a][2, c]));
                        Console.Write(' ');
                        Console.ResetColor();
                    }
                }
                Console.WriteLine();

                //Fourth Row of the map.
                for (int b = 0; b <= X; b++)
                {
                    for (int c = 0; c <= 4; c++)
                    {
                        Console.Write(checkCharacter(roomMap[b, a][3, c]));
                        Console.Write(' ');
                        Console.ResetColor();
                    }
                }
                Console.WriteLine();

                //Last Row of the map.
                for (int b = 0; b <= X; b++)
                {
                    for (int c = 0; c <= 4; c++)
                    {

                        Console.Write(checkCharacter(roomMap[b, a][4, c]));
                        Console.Write(' ');
                        Console.ResetColor();
                    }
                }
                Console.WriteLine();
            }
        }

        private char checkCharacter(char character)
        {
            if (character == '#')
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }

            if (character == '^')
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }

            if (character == '$')
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
            }

            if (character == '&')
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
            }

            if (character == '@')
            {
                Console.ForegroundColor = ConsoleColor.Blue;
            }
            return character;
        }
    }
}