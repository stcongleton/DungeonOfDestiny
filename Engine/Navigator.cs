﻿using System;
using Properties;

namespace Engine
{
    class Navigator
    {
        string direction { get; set; } = "";
        public string message { get; set; } = "";
        int currentY, currentX, X, Y;
        static Settings s = new Settings();
        public Navigator()
        {
            //Initiates the class. Nothing needs to be done here.
        }

        //The main navigation method. This is what the engine will call to make things happen.
        //Requires an array of rooms and a map.
        public void navigation(Room[,] map, Map m)
        {
            direction = "";
            currentY = m.currentY;
            currentX = m.currentX;
            X = s.maxX - 1;
            Y = s.maxY - 1;
            
            //Ask the user which direction they want to move. 
            Console.Write("Would you like to move North, South, East, or West?: ");
            string dir = Console.ReadLine();
            //Make sure direction isn't an empty string.
            if (dir == "")
            {
                //Tell the user they need to input a direction.
                message = "Please input a direction";
            }
            else
            {
                //Parse out the first character of the string in lower case. 
                direction = dir.ToString().Substring(0, 1).ToLower();
            }
            
            
            //Check for valid input and try to make the movement happen.
            //Also checks to make sure they aren't on an edge of the map.

            //Move north?
            if (direction == "n" && currentY != 0)
            {
                //Try to make the move
                if (move(map[currentY, currentX], map[currentY - 1, currentX], 'n'))
                {
                    //if it succeeds, move the player north.
                    m.currentY -= 1;
                }

            }

            //Move south?
            else if (direction == "s" && currentY != Y)
            {
                //Try to make the move
                if (move(map[currentY, currentX], map[currentY + 1, currentX], 's'))
                {
                    //if it succeeds, move the player south.
                    m.currentY += 1;
                }

            }

            //Move east?
            else if (direction == "e" && currentX != X)
            {
                //Try to make the move
                if (move(map[currentY, currentX], map[currentY, currentX + 1], 'e'))
                {
                    //if it succeeds, move the player east.
                    m.currentX += 1;
                }

            }

            //Move west?
            else if (direction == "w" && currentX != 0)
            {
                //Try to make the move
                if (move(map[currentY, currentX], map[currentY, currentX - 1], 'w'))
                {
                    //if it succeeds, move the player west.
                    m.currentX -= 1;
                }

            }

            //If the input is invalid, or the user tries to move off the map
            //Set the message they will see.
            else
            {
                message = "Please pick a valid direction (N,S,E,W).\nAlso, try not to leave the map.";
            }

        }

        //Overloaded navigation with Direction.
        public void navigation(Room[,] map, Map m, String dir)
        {
            currentY = m.currentY;
            currentX = m.currentX;
            X = s.maxX - 1;
            Y = s.maxY - 1;

            if (dir == "")
            {
                //Tell the user they need to input a direction.
                message = "Please input a direction";
            }
            else
            {
                //Parse out the first character of the string in lower case. 
                direction = dir.ToString().Substring(0, 1).ToLower();
            }


            //Check for valid input and try to make the movement happen.
            //Also checks to make sure they aren't on an edge of the map.


            //Move north?
            if (direction == "n" && currentY != 0)
            {
                //Try to make the move
                if (move(map[currentY, currentX], map[currentY - 1, currentX], 'n'))
                {
                    //if it succeeds, move the player north.
                    m.currentY -= 1;
                }

            }

            //Move south?
            else if (direction == "s" && currentY != Y)
            {
                //Try to make the move
                if (move(map[currentY, currentX], map[currentY + 1, currentX], 's'))
                {
                    //if it succeeds, move the player south.
                    m.currentY += 1;
                }

            }

            //Move east?
            else if (direction == "e" && currentX != X)
            {
                //Try to make the move
                if (move(map[currentY, currentX], map[currentY, currentX + 1], 'e'))
                {
                    //if it succeeds, move the player east.
                    m.currentX += 1;
                }

            }

            //Move west?
            else if (direction == "w" && currentX != 0)
            {
                //Try to make the move
                if (move(map[currentY, currentX], map[currentY, currentX - 1], 'w'))
                {
                    //if it succeeds, move the player west.
                    m.currentX -= 1;
                }

            }

            //If the input is invalid, or the user tries to move off the map
            //Set the message they will see.
            else
            {
                message = "Please pick a valid direction (N,S,E,W).\nAlso, try not to leave the map.\n";
            }

        }

        public bool move(Room leaving, Room entering, char direction)
        {
            //Check to see if there is an exit in the right direction.
            if (!checkExit(leaving, direction))
            {
                //If not, tell the user there are no exits that way.
                message = "There are no exits that way!\n";
                //Fail the move.
                return false;
            }
            //Reset the message to nothing on a successful move.
            message = "";
            
            //Leave the room the player is in.
            leaveRoom(leaving);

            //Enter the room the player is entering.
            enterRoom(entering);

            //Return a success!
            return true;
        }

        //Leave the room!
        //
        void leaveRoom(Room room)
        {
            //Tell the room that you're leaving.
            room.isOccupied = false;
            //Tell the room that you've been there.
            room.isVisited = true;
        }

        void enterRoom(Room room)
        {
            room.isOccupied = true;
            room.isVisited= true;
        }

        bool checkExit(Room room, char direction)
        {
            switch (direction)
            {
                case 'n':
                    if (!room.North)
                    {
                        return false;
                    }
                    break;
                case 's':
                    if (!room.South)
                    {
                        return false;
                    }
                    break;
                case 'e':
                    if (!room.East)
                    {
                        return false;
                    }
                    break;
                case 'w':
                    if (!room.West)
                    {
                        return false;
                    }
                    break;
                case '1':
                    Console.Clear();
                    Console.WriteLine("Broken!");
                    Console.ReadLine();
                    break;
               
                default:
                    return false;
            }
            return true;
        }

    }
}
