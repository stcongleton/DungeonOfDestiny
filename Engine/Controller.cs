﻿using System;

namespace Engine
{
    class Controller
    {
        //This class will have all of the methods to CONTROL EVERYTHING.

        //Variables
        public Room[,] map { get; set; }
        public Map m { get; set; }
        public Navigator nav { get; set; }
        Eyes eyes;
        string decision = "";
        string message = "";
        string comMessage = "";
        Player character;
        Combat com;
        bool lost, won, levelChange, attack;
        bool bosscheck = false;
        int level, itemNum;
        int X, Y, itemsGathered, monstersKilled, cheatsUsed, levelsWon, cont, condition;
        int[] score = new int[7];

        //Constructors
        public Controller(int maxX, int maxY, int maxMonster, int maxItems)
        {
            //Set the upper X and Y Bounds
            X = maxX;
            Y = maxY;

            //score initializers
            itemsGathered = 0;
            monstersKilled = 0;
            cheatsUsed = 0;
            levelsWon = 0;
            cont = 1;
            condition = 0;
            calcScore();

            //Make the Map.
            m = new Map(maxX, maxY, maxMonster, maxItems);
            map = m.map;

            //Make the navigator
            nav = new Navigator();

            //Make your eyes.
            eyes = new Eyes();

            //Start the engines of war.
            com = new Combat();

            won = false;
            lost = false;
            levelChange = false;
            attack = false;
        }

        public int[] mainLoop(Navigator nav, Map m, Room[,] map, int lvl, Player player)
        {
            int x5 = X * 5;
            character = player;
            level = lvl;
            m.printMap();
            do {
                lost = character.isDead;
                if (character.isDead)
                {
                    continue;
                }
                Console.Clear();
                m.printMap();
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                for (int i = 0; i < x5; i++)
                {
                    Console.Write("# ");
                }
                Console.WriteLine();
                Console.ResetColor();
                Console.WriteLine();
                if (nav.message.Length > 1)
                {
                    Console.WriteLine(nav.message);
                }
                if (comMessage.Length > 1)
                {
                    Console.WriteLine(comMessage);
                }
                if (message.Length > 1)
                {
                    Console.WriteLine(message);
                }
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                for (int i = 0; i < x5; i++)
                {
                    Console.Write("# ");
                }
                Console.WriteLine();
                Console.ResetColor();
                message = "";
                comMessage = "";
                Console.Write("HP: " + character.hitPoints);
                Console.Write(" Armor: " + character.AC);
                Console.Write(" Str: " + character.strength);
                Console.Write(" Dex: " + character.dexterity + "\n");
                Console.WriteLine("Level: " + level);

                if (map[m.currentY, m.currentX].boss && !bosscheck)
                {
                    Console.Write("Would you like to fight the boss? y/n ");
                    String bossFight = Console.ReadLine();
                    if (bossFight.Substring(0,1).ToLower() == "y")
                    {
                        bosscheck = true;
                        comMessage = com.attackBoss(map[m.currentY, m.currentX].bossReference, character);
                        if (map[m.currentY, m.currentX].bossReference.checkDeath())
                        {
                            map[m.currentY, m.currentX].boss = false;
                            levelChange = true;
                            won = true;
                            monstersKilled += 1;
                            levelsWon += 1;
                        }
                        if (character.hitPoints <= 0)
                        {
                            character.kill(true);
                        }
                        continue;
                    }
                    else
                    {
                        bosscheck = true;
                        continue;
                    }

                }
                if (attack)
                {
                    if (map[m.currentY, m.currentX].monster)
                    {
                       comMessage = com.attackPlayer(map[m.currentY, m.currentX].monsterReference, character);
                        if (map[m.currentY, m.currentX].monsterReference.checkDeath())
                        {
                            map[m.currentY, m.currentX].monster = false;
                            monstersKilled += 1;
                        }
                        if (character.hitPoints <= 0)
                        {
                            character.kill(true);
                        }
                    }

                    if (map[m.currentY, m.currentX].boss)
                    {
                        comMessage = com.attackPlayer(map[m.currentY, m.currentX].bossReference, character);
                        if (map[m.currentY, m.currentX].bossReference.checkDeath())
                        {
                            map[m.currentY, m.currentX].boss = false;
                            levelChange = true;
                            won = true;
                            monstersKilled += 1;
                            levelsWon += 1;
                        }
                        if (character.hitPoints <= 0)
                        {
                            character.kill(true);
                        }
                    }
                }

                decision = getInputFor("What would you like to do?: ");
                evaluateChoice(decision);
            }while (decision.Substring(0, 1).ToLower() != "q" && !lost && !won && !levelChange);
            if (lost)
            {
                cont = 0;
                condition = 1;
                calcScore();
                return score;
            }

            if (level >= 5)
            {
                if (won)
                {
                    Console.Clear();
                    cont = 0;
                    calcScore();
                    return score;
                }
            }
            if (levelChange && won)
            {
                Console.Clear();
                Console.ReadLine();
                level += 1;
                condition = 4;
                calcScore();
                return score;

            }
            Console.Clear();
            cont = 0;
            condition = 2;
            calcScore();
            return score;
        }

        void evaluateChoice(string choice)
        {
            if (choice.Length >= 4)

            {
                string decision = choice.Substring(0, 4).ToLower();

                switch (decision)
                {
                    //Move
                    case "move":
                        attack = false;
                        if (choice.Length > 5) {
                            string dir = choice.Substring(5, 1).ToLower();
                            if ( dir == "n" || dir == "e" || dir == "s" || dir == "w")
                            {
                                nav.navigation(map, m, dir);
                                if(map[m.currentY, m.currentX].monster)
                                {
                                    if (map[m.currentY, m.currentX].monsterReference.isAggressive)
                                    {
                                        attack = true;
                                        comMessage = com.attackPlayer(map[m.currentY, m.currentX].monsterReference, character);
                                    }

                                    if (map[m.currentY, m.currentX].monsterReference.checkDeath())
                                    {
                                        map[m.currentY, m.currentX].monster = false;
                                        monstersKilled += 1;
                                    }
                                    if (character.hitPoints <= 0)
                                    {
                                        character.kill(true);
                                    }
                                }


                            }
                            else
                            {
                                nav.navigation(map, m);
                                if (map[m.currentY, m.currentX].monster)
                                {
                                    if (map[m.currentY, m.currentX].monsterReference.isAggressive)
                                    {
                                        attack = true;
                                        comMessage = com.attackPlayer(map[m.currentY, m.currentX].monsterReference, character);
                                    }

                                   if (map[m.currentY, m.currentX].monsterReference.checkDeath())
                                   {
                                       map[m.currentY, m.currentX].monster = false;
                                        monstersKilled += 1;
                                    }
                                   if (character.hitPoints <= 0)
                                   {
                                       character.kill(true);
                                   }
                                }


                            }
                        }
                        else
                        {
                            nav.navigation(map, m);
                            if (map[m.currentY, m.currentX].monster)
                            {
                                if (map[m.currentY, m.currentX].monsterReference.isAggressive)
                                {
                                    attack = true;
                                    comMessage = com.attackPlayer(map[m.currentY, m.currentX].monsterReference, character);
                                }
                                if (map[m.currentY, m.currentX].monsterReference.checkDeath())
                                {
                                    map[m.currentY, m.currentX].monster = false;
                                    monstersKilled += 1;
                                }
                                if (character.hitPoints <= 0)
                                {
                                    character.kill(true);
                                }
                            }
                            }
                        break;

                    //Attack
                    case "atta":
                        if (map[m.currentY, m.currentX].monster)
                        {
                            attack = true;
                            comMessage = com.attackMonster(map[m.currentY, m.currentX].monsterReference, character);
                            if (map[m.currentY, m.currentX].monsterReference.checkDeath())
                            {
                                map[m.currentY, m.currentX].monster = false;
                                monstersKilled += 1;
                            }
                        }

                        else if (map[m.currentY, m.currentX].boss)
                        {
                            attack = true;
                            comMessage = com.attackBoss(map[m.currentY, m.currentX].bossReference, character);
                            if (map[m.currentY, m.currentX].bossReference.checkDeath())
                            {
                                map[m.currentY, m.currentX].boss = false;
                                monstersKilled += 1;
                                levelChange = true;
                                won = true;
                                levelsWon += 1;
                            }
                        }
                        else
                        {
                            message = "There is nothing here to attack!";
                        }

                        if (character.hitPoints <= 0)
                        {
                            character.kill(true);
                        }
                        break;

                    //look
                    case "look":
                        message = eyes.Look(map[m.currentY,m.currentX]);
                        break;

                    //pick up
                    case "pick":
                        if (character.bag.getLength() >= 10)
                        {

                            message = "Inventory full, drop some items to pick up more!";
                        }
                        else
                        {
                            if (choice.Length > 8 && int.TryParse(choice.Substring(8, 1), out itemNum))
                            {
                                character.bag.add(map[m.currentY, m.currentX].contentsReference[itemNum - 1]);
                                map[m.currentY, m.currentX].contentsReference.Remove(map[m.currentY, m.currentX].contentsReference[itemNum - 1]);
                                itemsGathered += 1;
                            }

                            else if (choice.Length > 8 && choice.Substring(8, 1) == "a")
                            {
                                while (map[m.currentY, m.currentX].contentsReference.Count >= 1)
                                {
                                        character.bag.add(map[m.currentY, m.currentX].contentsReference[map[m.currentY, m.currentX].contentsReference.Count -1]);
                                        map[m.currentY, m.currentX].contentsReference.Remove(map[m.currentY, m.currentX].contentsReference[map[m.currentY, m.currentX].contentsReference.Count - 1]);
                                        itemsGathered += 1;
                                }

                            }

                            else if (choice.Length > 5 && int.TryParse(choice.Substring(5, 1), out itemNum))
                            {
                                character.bag.add(map[m.currentY, m.currentX].contentsReference[itemNum - 1]);
                                map[m.currentY, m.currentX].contentsReference.Remove(map[m.currentY, m.currentX].contentsReference[itemNum - 1]);
                                itemsGathered += 1;
                            }
                            else
                            {
                                Console.Write("Which item would you like to pick up?: ");
                                int id = 0;
                                int.TryParse(Console.ReadLine(), out id);
                                character.bag.add(map[m.currentY, m.currentX].contentsReference[id - 1]);
                                map[m.currentY, m.currentX].contentsReference.Remove(map[m.currentY, m.currentX].contentsReference[id - 1]);
                                itemsGathered += 1;
                            }

                            if (map[m.currentY, m.currentX].contentsReference.Count == 0)
                            {
                                map[m.currentY, m.currentX].contents = false;
                            }
                        }

                        break;

                    //open Inventory
                    case "inve":
                        character.bag.printInventory();
                        break;

                    //help
                    case "help":
                        message += "Valid options: \n";
                        message += "*Inventory - Shows player inventory. \n";
                        message += "*Pick Up, Pick Up <item number>, Pick Up All - picks up the designated item in the room. \n";
                        message += "*Move, Move <north, south, east, west> - Moves character in the given direction.\n";
                        message += "*Look - Shows the items and creatures in the room.\n";
                        message += "*Equip <item number> - Equips the designated item in the inventory or uses a consumable item.\n";
                        message += "*UnEquip <item number> - Unequips the designated item from the player. \n";
                        message += "*Character - Gets the character info. \n";
                        message += "*Drop - Drops the item from the inventory into the room. \n";
                        message += "*Quit - Quits the game.";
                        break;

                    //Character info.
                    case "char":
                        message = character.getCharacter();
                        break;

                    //equip: consumable items go in here.
                    case "equi":
                        if (choice.Length > 6)
                        {
                            string dir = choice.Substring(6, 1).ToLower();
                            int value;
                            int.TryParse(dir, out value);
                            if ( value > 0)
                            {
                                if(character.bag.getItem(int.Parse(dir) - 1).isConsumable)
                                {
                                    message = "You just used " + character.bag.getItem(int.Parse(dir) - 1).Name;
                                    character.AC += character.bag.getItem(int.Parse(dir) - 1).ArmorBonus;
                                    character.strength += character.bag.getItem(int.Parse(dir) - 1).DamageBonus;
                                    character.dexterity += character.bag.getItem(int.Parse(dir) - 1).HitBonus;
                                    character.constitution += character.bag.getItem(int.Parse(dir) - 1).HealthBonus;
                                    character.hitPoints += character.bag.getItem(int.Parse(dir) - 1).HitPoints;
                                    character.bag.drop(character.bag.getItem(int.Parse(dir) - 1));

                                }
                                else
                                {
                                    message = character.bag.equipItem(int.Parse(dir) - 1, character, character.bag.getItem(int.Parse(dir) - 1).type);
                                }
                            }
                            else
                            {
                                message = "Please pick a valid item!";
                            }
                        }
                        else
                        {
                            message = "Use format 'equip <item number>.\n I.E. equip 1";
                        }
                        break;

                    //Unequip
                    case "uneq":
                        if (choice.Length > 8)
                        {
                            string dir = choice.Substring(8, 1).ToLower();
                            int value;
                            int.TryParse(dir, out value);
                            if (value > 0)
                            {
                                message = character.bag.unequipItem(int.Parse(dir) - 1, character, character.bag.getItem(int.Parse(dir) - 1).type);
                            }
                            else
                            {
                                message = "Please pick a valid item!";
                            }
                        }
                        else
                        {
                            message = "Use format 'unequip <item number>.\n I.E. unequip 1";
                        }
                        break;

                    //fuck
                    case "fuck":
                        message = "If you would like to quit. \n Simply type quit.";
                        character.constitution = 1000;
                        character.hitPoints += 200;
                        cheatsUsed += 1;
                        break;

                    //drop
                    case "drop":

                        if (choice.Length > 5)
                        {
                            string dir = choice.Substring(5, 1).ToLower();
                            int value;
                            int.TryParse(dir, out value);
                            if (value > 0)
                            {
                                if(character.bag.getItem(int.Parse(dir) -1 ).Equipped == "false")
                                {
                                    message = "You dropped " + character.bag.getItem(int.Parse(dir) - 1).Name;
                                    if (map[m.currentY, m.currentX].contentsReference.Count < 5)
                                    {
                                        map[m.currentY, m.currentX].contentsReference.Add(character.bag.getItem(int.Parse(dir) - 1));
                                    }
                                    character.bag.drop(character.bag.getItem(int.Parse(dir) - 1));
                                }
                                else
                                {
                                    character.bag.getItem(int.Parse(dir) - 1).Equipped = "false";
                                    message = "You dropped " + character.bag.getItem(int.Parse(dir) - 1).Name;
                                    if (map[m.currentY, m.currentX].contentsReference.Count < 5)
                                    {
                                        map[m.currentY, m.currentX].contentsReference.Add(character.bag.getItem(int.Parse(dir) - 1));
                                    }
                                    character.bag.drop(character.bag.getItem(int.Parse(dir) - 1));
                                }

                            }
                            else
                            {
                                message = "Please pick a valid item!";
                            }
                        }
                        else
                        {
                            message = "Use format 'drop <item number>.\n I.E. drop 1";
                        }
                        break;

                    //Other unsupported actions
                    default:
                        message = "Please pick a valid option! \n Type HELP for help.";
                        break;
                }
            }
            else if ( choice.Substring(0, 1).ToLower() == "q")
            {
                Console.Clear();
                cont = 0;
                condition = 2;
                calcScore();
                return;
            }
            else
            {
                message = "Please pick a valid option! \n Type HELP for help.";
            }


        }
        private void calcScore()
        {
            score[0] = level;
            score[1] = itemsGathered;
            score[2] = monstersKilled;
            score[3] = levelsWon;
            score[4] = cheatsUsed;
            score[5] = cont;
            score[6] = condition;
        }
        private string getInputFor(string content) {
            string input;
            do {
                Console.Write(content);
                input = Console.ReadLine();
            }while (string.IsNullOrWhiteSpace(input));
            return input;
        }
    }
}
