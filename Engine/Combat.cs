﻿using System;

namespace Engine
{
    class Combat
    {
        Random rand;
        public Combat()
        {
            rand = new Random(Guid.NewGuid().GetHashCode());
        }
        /// <summary>
        /// attackMonster
        /// A method to have the player attack the monster.
        /// </summary>
        /// <param name="monster">Monster object</param>
        /// <param name="player">Player object</param>
        public string attackMonster(Monster monster, Player player)
        {
            int playerToHit = rand.Next(1, 20);
            string result = "";
            playerToHit += player.dexterity;
            int damage = rand.Next(1, player.strength);
            if (playerToHit > monster.AC)
            {
                result += "You hit the " + monster.monsterName + "!\n";
                result += "The attack delt " + damage + " damage!\n";
                monster.hitPoints -= damage;
            }
            else
            {
                result += "Your attack missed!\n";
            }


            // If the monster has hit points left attack the player, otherwise the monster will not attack the player.
            if (monster.hitPoints > 0)
            {
                result += attackPlayer(monster, player);
            }
            else
            {
                result += "You have killed the " + monster.monsterName + "!";
            }
            
            return result;

        }

        /// <summary>
        /// attackBoss
        /// A method to have the player attack the boss.
        /// </summary>
        /// <param name="monster">Monster object</param>
        /// <param name="player">Player object</param>
        public string attackBoss(Monster monster, Player player)
        {
            int playerToHit = rand.Next(1, 20);
            string result = "";
            playerToHit += player.dexterity;
            int damage = rand.Next(1, player.strength);
            if (playerToHit > monster.AC)
            {
                result += "You hit the " + monster.monsterName + "!\n";
                result += "The attack delt " + damage + " damage!\n";
                monster.hitPoints -= damage;
            }
            else
            {
                result += "Your attack missed!\n";
            }


            // If the monster has hit points left attack the player, otherwise the monster will not attack the player.
            if (monster.hitPoints > 0)
            {
                result += attackPlayer(monster, player);
            }
            else
            {
                result += "You have killed the " + monster.monsterName + "!";
            }

            return result;

        }

        /// <summary>
        /// attackPlayer
        /// A method to have the monster attack the player
        /// </summary>
        /// <param name="monster"></param>
        /// <param name="player"></param>
        public string attackPlayer(Monster monster, Player player)
        {
            int monsterToHit = rand.Next(1, 20);
            string result = "";
            monsterToHit += monster.toHit;
            int damage = rand.Next(1, monster.attackPower);
            if (monsterToHit > player.AC)
            {
                result += "The "+monster.monsterName+" hit you!\n";
                result += "The attack delt " + damage + " damage!\n";
                player.hitPoints -= damage;
                player.checkHP();
            }
            else
            {
                result += "The " + monster.monsterName + "'s attack missed!\n";
            }
            
            return result;
        }

    }
}
