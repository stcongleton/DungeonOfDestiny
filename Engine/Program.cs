﻿using Properties;
using System;
using System.IO;
using System.Xml;


namespace Engine
{
    class Program
    {

        static void Main(string[] args)
        {
            int itemsGathered = 0, monstersKilled = 0, cheatsUsed = 0, levelsWon = 0, cont = 1, condition = 0;
            int[] score = new int[6];
            AppDomain.CurrentDomain.UnhandledException +=
                new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            Settings s = new Settings();
            int level = 1;
            int lvlModified;
            Controller c;
            Player character;
            printFluff("start");
            Console.Write("What would you like your player's name to be (10 Character max)?: ");
            string name = Console.ReadLine();

            if (name.Length > 10)
            {
                name = name.Substring(0, 10);
            }
            character = new Player(name.Trim());

            while (level <= s.maxLevel && cont == 1)
            {
                lvlModified = level * 10;
                if (lvlModified <= s.maxItems)
                {
                    s.maxItems = lvlModified;
                }

                if (lvlModified <= s.maxMonster)
                {
                    s.maxMonster = lvlModified;
                }

                c = new Controller(s.maxX,s.maxY, s.maxMonster, s.maxItems);
                score = c.mainLoop(c.nav, c.m, c.map, level, character);
                level = score[0];
                itemsGathered += score[1];
                monstersKilled += score[2];
                levelsWon += score[3];
                cheatsUsed += score[4];
                cont = score[5];
                condition = score[6];
            }
            if (score[4] > 5)
            {
                condition = 3;
            }
            switch (condition)
            {
                case 0:
                    printFluff("won");
                    break;

                case 1:
                    printFluff("lost");
                    break;

                case 2:
                    printFluff("quit");
                    break;

                case 3:
                    printFluff("cheated");
                    break;

                case 4:
                    printFluff("nextlevel");
                    break;
            }
            Console.WriteLine("Your Scores:");
            if (cheatsUsed > 5) {
                Console.WriteLine("You cheated too much. You don't get a score!");
                Console.WriteLine("Press any key to quit.");
            }
            else
            {
                Console.WriteLine("Items Gathered: " + itemsGathered);
                Console.WriteLine("Monsters Killed: " + monstersKilled);
                Console.WriteLine("Levels Won: " + levelsWon);
                Console.WriteLine("Cheats Used: " + cheatsUsed);
                Console.WriteLine("Press any key to quit.");
            }
            Console.ReadLine();

        }

        static void CurrentDomain_UnhandledException (object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Console.WriteLine("EXCEPTION!");
                Console.ReadLine();
                Exception ex = (Exception)e.ExceptionObject;
                StreamWriter log;
                if (!File.Exists("logfile.txt"))
                {
                    log = new StreamWriter("logfile.txt");
                }
                else
                {
                    log = File.AppendText("logfile.txt");
                }

                log.WriteLine("Data Time:" + DateTime.Now);
                log.WriteLine("Exception Name:" + ex.Message);
                log.WriteLine("Stacktrace:" + ex.StackTrace);
                log.WriteLine();
                log.WriteLine();
                log.Close();
            }
            finally
            {
                Environment.Exit(0);
            }
        }

        static void printFluff(string fluff)
        {
            XmlReader reader = XmlReader.Create("Data/fluff.xml");
            while (reader.Read())
            {
                if (reader.IsStartElement())
                {
                    if (reader.Name.ToString() == fluff)
                    {
                        Console.WriteLine(reader.ReadString());
                    }
                }

            }
        }
    }
}
