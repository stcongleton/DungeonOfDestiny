﻿using System.Collections.Generic;

namespace Engine
{
    class Eyes
    {
        string sight;
        /// <summary>
        ///  Constructor for eyes. Eyes need nothing but to open.
        /// </summary>
        public Eyes()
        {
            //Eyes need nothing but to open.
        }

        public string Look(Room room)
        {
            sight ="You see:\n";
            if (room.contents)
            {
                int count = 0;
                List<Item> reference = room.contentsReference;
                foreach (Item i in reference)
                {
                    count++;
                    sight += "An Item (#" + count + "): " + i.Name + ".\n";
                }
                
            }
            if (room.monster)
            {
                sight += "A " + room.monsterReference.monsterName + "!\n";
            }
            if (room.boss)
            {
                sight += "A "+room.bossReference.monsterName.ToUpper()+"!\n";
            }
            if (!room.contents && !room.monster && !room.boss)
            {
                sight += "Nothing.\n";
            }

            return sight;
        }
      
    }
}
