﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Engine
{
    public class Monster
    {
        public String monsterName { get; set; }
        public int hitPoints { get; set; }
        public int attackPower { get; set; }
        public int AC { get; set; }
        public int toHit { get; set; }
        public bool isAggressive { get; set; }
        List<string> attributes = new List<string>();



        /* Database access parameter to tell which monster it is, and also get values for hitPoints and attackPower */
        /* Placeholder values for now until database is running */
        public Monster(/* Will be a database access parameter */)
        {
            /* All of these Monster properties will be set by the values in the database */
            monsterName = "Fuckwad";
            hitPoints = 3;
            attackPower = 5;
            toHit = 3;
            AC = 10;
            isAggressive = false;
        }

        public Monster(int id)
        {
            XDocument reader = XDocument.Load("Data/monsters.xml");
            IEnumerable<XElement> monster =
                (from itm in reader.Root.Descendants("Monster")
                 where (int)itm.Attribute("id") == id
                 select itm);
            monsterName = monster.Elements("MonsterName").First().Value.ToString();
            toHit = int.Parse(monster.Elements("ToHit").First().Value.ToString());
            attackPower = int.Parse(monster.Elements("AttackPower").First().Value.ToString());
            AC = int.Parse(monster.Elements("AC").First().Value.ToString());
            isAggressive = bool.Parse(monster.Elements("Aggressive").First().Value.ToString());
            hitPoints = int.Parse(monster.Elements("HitPoints").First().Value.ToString());

        }


        public bool checkDeath()
        {
            if (hitPoints <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /* Utility methods for a Monster */
        /* TODO */
        /* Make dropItem method */
        /* Make dropCurrency method */
    }
}
