﻿using System;
using System.Collections.Generic;
using Properties;

namespace Engine
{
    public class Room
    {
        Settings s = new Settings();
        //Location variables for placement of the room on a map.
        int xLocation;  
        int yLocation;

        int maxMonster;
        int maxItem;

        //Exit variables to determine if there is an exit to a given direction.
        public bool North { get; set; } = false;
        public bool East { get; set; } = false;
        public bool South { get; set; } = false;
        public bool West { get; set; } = false;

        //Variables to determine if the room has items inside, monsters inside,
        //or is the boss room.
        public bool contents { get; set; }
        public List<Item> contentsReference { get; } = new List<Item>();
        public bool monster { get; set; }
        public Monster monsterReference { get; set; }
        public Monster bossReference { get; set; }
        public bool boss { get; set; }
        public bool pathToStart { get; set; } = false;
        public bool isOccupied { get; set; } = false;
        public bool isVisited { get; set; } = false;
        public Random rand = new Random(Guid.NewGuid().GetHashCode());

        //Default Constructor
        public Room(int x, int y, int MaxMonster, int MaxItem)
        {
            maxItem = MaxItem;
            maxMonster = MaxMonster;
            xLocation = x;
            yLocation = y;

            //Randomly generate what opening the room has. 
            //Each room will have exactly one exit.
            for (int r = 0; r <= 2; r++)
            {
                int doorLocation = rand.Next(1, 100);
                if (doorLocation < 25)
                {
                    North = true;
                }
                if (doorLocation > 25 && doorLocation < 50)
                {
                    South = true;
                }
                if (doorLocation > 50 && doorLocation < 75)
                {
                    East = true;
                }
                if (doorLocation > 75)
                {
                    West = true;
                }

            }
            //Randomly determine if the room has contents.
            roomContents(rand);

            //Randomly determine if there is a monster in the room.
            //More likely to have monster if there are contents in the room.
            roomMonster(rand);

            if (xLocation == 0 && yLocation == 0)
            {
                isOccupied = true;
                monster = false;
                contents = false;
                isVisited = true;
            }

        }


        //Other Methods
        //These do shit.

        public char[,] generateRoom()
        {
            char[,] room = new char[5, 5];

            //Make all the outside walls look like walls.
            //* * * * * 
            //*       *
            //*       *
            //*       *
            //* * * * *
            for (int i = 0; i <= 4; i++)
            {
                room[0, i] = '*';
                room[i, 0] = '*';
                room[4, i] = '*';
                room[i, 4] = '*';
            }

            //Fill the room with NOTHING.
            for (int i = 1; i <= 3; i++)
            {
                for (int j = 1; j <= 3; j++)
                {
                    room[i, j] = ' ';
                }
                
            }

            

            //Open the doors that are open in the room!
            //Also count them.

            if (North == true)
            {
                room[0, 2] = '#';                
            }
            if (South == true)
            {
                room[4, 2] = '#';
            }
            if (East == true)
            {
                room[2, 4] = '#';              
            }
            if (West == true)
            {
                room[2, 0] = '#';                
            }

            if (monster)
            {
                room[1, 1] = '^';
            }

            if (contents)
            {
                room[1, 3] = '$';
            }

            if (boss)
            {
                room[3, 3] = '&';
            }

            if (isOccupied)
            {
                room[2, 2] = '@';
            }

            if (!isOccupied)
            {
                room[2, 2] = ' ';
            }

            //If there's no path to start, don't show the room at all.
            if (!pathToStart || !isVisited)
            {
                for (int i = 0; i <= 4; i++)
                {
                    for (int j = 0; j <= 4; j++)
                    {
                        room[i, j] = '*';
                    }

                }
            }


            return room;
        }

        private void roomContents(Random rand)
        {
            //There is a 33% chance of an item in the room.
            int hasContents = rand.Next(1, 100);
            if (hasContents < 34)
            {
                contents = true;
                contentsReference.Add(new Item(rand.Next(1, maxItem)));
                contentsReference.Add(new Item(rand.Next(1, maxItem)));

            }
            else
            {
                contents = false;
            }    
        }

        private void roomMonster(Random rand)
        {
            //Figure out what the percentage is
            int hasMonster = rand.Next(1, 100);

            //Check to see if there is treasure for the monster to be guarding.
            if (contents == true && hasMonster > 25)
            {
                monster = true;
                monsterReference = new Monster(rand.Next(1, maxMonster));
            }
            else if (hasMonster > 80)
            {
                monster = true;
                monsterReference = new Monster(rand.Next(1, maxMonster));

            }
            else
            {
                monster = false;
            }
        }

        public void makeBoss()
        {
            bossReference = new Monster(rand.Next(1, maxMonster));
            bossReference.hitPoints += rand.Next(1,5) * (maxMonster / 10);
            bossReference.AC += 1 * (maxMonster / 10);
        }

    }
}
