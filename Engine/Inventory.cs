﻿using System;
using System.Collections.Generic;

namespace Engine
{
    class Inventory
    {
        List<Item> bag;

        public Inventory()
        {
            bag = new List<Item>();
        }

        public void add(Item item)
        {
            bag.Add(item);
        }

        public void drop(Item item)
        {
            bag.Remove(item);
        }

        public int getLength()
        {
            return bag.Count;
        }

        public string info(Item item)
        {
            string about = item.debug();
            return about;
        }

        public void printInventory()
        {
            Console.Clear();
            int count = 1;
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("# # # # # # # # # # # # # # # # # # # # # # # # #");
            Console.ResetColor();
            foreach (Item i in bag)
            {
                Console.WriteLine();
                Console.WriteLine("Item " + count + ":");
                Console.Write(i.debug());
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("# # # # # # # # # # # # # # # # # # # # # # # # #");
                Console.ResetColor();
                count++;
            }
            Console.WriteLine("Press any key to go back");
            Console.ReadLine();
            return;
        }

        public string equipItem(int id, Player character, int type)
        {

            if (id > bag.Count)
            {
                return "No Such item\n";
            }
            else if (type == 1 && character.hasItem)
            {
                return "You already have an item equipped!\n";
            }
            else if (type == 3 && character.hasArmor)
            {
                return "You already have armor equipped!\n";
            }
            else if (type == 2 && character.hasWeapon)
            {
                return "You already have a weapon equipped!\n";
            }
            else
            {
                bag[id].Equipped = "TRUE";

                switch (type)
                {
                    case 1:
                        character.equippedItem = bag[id].Name;
                        break;

                    case 2:
                        character.equippedWeapon = bag[id].Name;
                        break;

                    case 3:
                        character.equippedArmor = bag[id].Name;
                        break;

                }
                character.constitution += bag[id].HealthBonus;
                character.strength += bag[id].DamageBonus;
                character.dexterity += bag[id].HitBonus;
                character.AC += bag[id].ArmorBonus;

                switch (type)
                {
                    case 1:
                        character.hasItem = true;
                        break;

                    case 2:
                        character.hasWeapon = true;
                        break;

                    case 3:
                        character.hasArmor = true;
                        break;

                }
                return "Item equipped.\n";
            }
        }
        public string unequipItem(int id, Player character, int type)
        {
            if (id > bag.Count)
            {
                return "No Such item\n";
            }
            else if (type == 1 && !character.hasItem)
            {
                return "You don't have an item equipped!\n";
            }
            else if (type == 2 && !character.hasWeapon)
            {
                return "You don't have a Weapon equipped!\n";
            }
            else if (type == 3 && !character.hasArmor)
            {
                return "You don't have armor equipped!\n";
            }
            else
            {
                bag[id].Equipped = "FALSE";
                switch (type)
                {
                    case 1:
                        character.equippedItem = "none";
                        break;

                    case 2:
                        character.equippedWeapon = "none";
                        break;

                    case 3:
                        character.equippedArmor = "none";
                        break;

                }
                character.constitution -= bag[id].HealthBonus;
                character.strength -= bag[id].DamageBonus;
                character.dexterity -= bag[id].HitBonus;
                character.AC -= bag[id].ArmorBonus;
                switch (type)
                {
                    case 1:
                        character.hasItem = false;
                        break;

                    case 2:
                        character.hasWeapon = false;
                        break;

                    case 3:
                        character.hasArmor = false;
                        break;

                }
                return "Item unequipped.\n";
            }
        }

        public Item getItem(int id)
        {
            
            return bag[id];
        }

    }
}
